import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { Platform, ToastController, AlertController, MenuController, ModalController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  username: string;
  pass: string;

  // credenciales: FormGroup;

  constructor(private authService: AuthService, private router: Router, public navCtrl: NavController) { }

  ngOnInit() {
    // this.credenciales = this.formBuilder.group({
    //   user: ['', Validators.required],
    //   pass: ['', Validators.required]
    // });
  }

  // login() {
  //   this.authService.loguearse(this.credenciales.value).subscribe( dato => {
  //     if (dato === true) {
  //       this.router.navigate(['inicio']);
  //     }
  //   });
  // }

  login() {
    this.authService.loguearse(this.username, this.pass).subscribe(
      resp => {
        if (resp === true) {
          console.log('LA RESPUESTA DEL SERVIDOR HA SIDO TRUE. VAMOS AL HOME');
          // this.router.navigateByUrl('/inicio');
          this.navCtrl.navigateRoot('/inicio');
        } else {
          this.username = '';
          this.pass = '';
        }
      }
    );
  }



}
