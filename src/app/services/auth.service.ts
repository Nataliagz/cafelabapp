import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { map, catchError } from 'rxjs/operators';
import { AlertController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import CryptoJS from 'crypto-js';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user = null;
  Local = 'NADA';
  token = null;
  authState = new BehaviorSubject(false);

  constructor(private http: HttpClient, private storage: Storage, public alertController: AlertController, private router: Router) {
    this.checkToken();
  }

  // ------------------------ ALERTAS ------------------------
  async alertError(texto) {
    const alert = await this.alertController.create({
      header: 'Error',
      message: texto,
      buttons: ['OK']
    });

    await alert.present();
  }

  // ------------------------ FIN ALERTAR ------------------------


  // ---------------------- INICIO DE SESIÓN ----------------------
  checkToken() {
    this.storage.get('TOKEN_KEY').then(token => {
      // Si el usuario está logueado existe el token
      this.token = token;
      this.storage.get('U').then((valor) => {
        this.user = valor;
        this.storage.get('LOCAL').then((val) => {
          this.Local = val;
          this.authState.next(true);
        });
      });
    });
  }

   loguearse(username, passw) {
    const URLresp = environment.url + '/Login';
    const obj = {
      User: btoa(username),
      Password: CryptoJS.SHA512(passw).toString()
    };

    console.log('VALOR DEL FORMULARIO' + JSON.stringify(obj));

    return this.http.post(URLresp, obj).pipe(map((resp: any) => {
      console.warn('RESPUETA DEL SERVIDOR SOBRE EL LOGUEO' + JSON.stringify(resp));

      switch (resp.CODE) {
          case '100':
              this.storage.set('U', obj.User); // Almacenamos el usuario en base64
              this.storage.set('TOKEN_KEY', resp.HASH); // Guardamos en storage el token
              if (resp.LOCAL) {
                this.storage.set('LOCAL', resp.LOCAL);
              } else {
                this.storage.set('LOCAL', 'NADA');
              }
              return true;

          case '400':
              console.log('ERROR DESCONOCIDO');
              const txt = 'Error desconocido. Pongase en contacto con el administrador';
              this.alertError(txt);
              return false;

          case '403':
              console.log('USUARIO DESCONOCIDO');
              const text = 'El usuario no existe';
              this.alertError(text);
              return false;

          case '404':
              console.log('PASSWORD NO ES CORRECTA');
              const tex = 'Contraseña incorrecta';
              this.alertError(tex);
              return false;

          default:
              break;
      }
    }));
  }


  // loguearse(credenciales) {
  //   const URLresp = environment.url + '/Login';
  //   const obj = {
  //     User: btoa(credenciales.user),
  //     Password: CryptoJS.SHA512(credenciales.pass).toString()
  //   };

  //   console.log('VALOR DEL FORMULARIO' + JSON.stringify(credenciales));

  //   return this.http.post(URLresp, obj).pipe(map((resp: any) => {

  //     switch (resp.CODE) {
  //         case '100':
  //             this.storage.set('U', obj.User); // Almacenamos el usuario en base64
  //             this.storage.set('TOKEN_KEY', resp.HASH); // Guardamos en storage el token
  //             if (resp.LOCAL) {
  //               this.storage.set('LOCAL', resp.LOCAL);
  //             } else {
  //               this.storage.set('LOCAL', 'NADA');
  //             }
  //             return true;

  //         case '400':
  //             console.log('ERROR DESCONOCIDO');
  //             const txt = 'Error desconocido. Pongase en contacto con el administrador';
  //             this.alertError(txt);
  //             return false;

  //         case '403':
  //             console.log('USUARIO DESCONOCIDO');
  //             const text = 'El usuario no existe';
  //             this.alertError(text);
  //             return false;

  //         case '404':
  //             console.log('PASSWORD NO ES CORRECTA');
  //             const tex = 'Contraseña incorrecta';
  //             this.alertError(tex);
  //             return false;

  //         default:
  //             break;
  //     }
  //   }));
  // }

  logout() {
    this.authState.next(false);
    console.warn('SE VA A BORRAR EL STORAGE');
    this.storage.remove('U');
    this.storage.remove('TOKEN_KEY').then(() => {
      this.authState.next(false);
    });
  }

  isAuthenticated() {
    return this.authState.value;
  }


  // Recuperar todos los locales
  allLocales() {
    const URLresp = environment.url + '/GetLocales';
    const obj = {
      User: this.user,
      Token: this.token
    };

    console.log(JSON.stringify(obj));
    return this.http.post(URLresp, obj).pipe(map((resp: any) => {
      return resp;
    }));
  }



}
