import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  local = '';
  itemsLocales: Array<any>;

  constructor(private authService: AuthService, private router: Router) {
    this.loadLocal();
  }

  ngOnInit() {
  }

  loadLocal() {
    // this.local = this.authService.Local;
    if (this.authService.Local !== 'NADA') {
      // Si el usuario tiene local, entonces saltamos esta página
      console.log(this.authService.Local);
      this.router.navigate(['/encuestas']);
    } else {
      console.log('NO TIENE LOCAL' + this.local);
      this.getLocales();
    }
  }

  getLocales() {
    this.authService.allLocales().subscribe(resp => {
      this.itemsLocales = resp.LOCALES;
    });
  }

  logout() {
    this.authService.logout();
  }

}
